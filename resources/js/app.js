import Vue from "vue";
import VueLazyload from 'vue-lazyload';
import {tns} from "../../node_modules/tiny-slider/src/tiny-slider";
import "../../node_modules/tiny-slider/dist/tiny-slider.css";

import ProductCard from "./vue/components/ProductCard";



// const files = require.context('./', true, /\.vue$/i, 'lazy').keys();
let __svg__ = {path: '../../resources/svg/**/*.svg', name: 'images/sprite.svg'};

Vue.use(VueLazyload);

// files.forEach(file => {
//   Vue.component(file.split('/').pop().split('.')[0], () => import(`${file}` /*webpackChunkName: "js/components/[request]" */));
// });

new Vue({
  el: '#app',
  components: {
    ProductCard,
    HeaderComponent: () => ({component: import('./vue/components/HeaderComponent' /* webpackChunkName: "js/components/header" */)}),
    InstagramList: () => ({component: import('./vue/components/InstagramList' /* webpackChunkName: "js/components/instagram-list" */)}),
    FooterComponent: () => ({component: import('./vue/components/FooterComponent' /* webpackChunkName: "js/components/footer" */)}),
  },
  mounted() {
    setTimeout(() => document.getElementById('preloader').classList.add('active'), 300);

    if (document.querySelector('.slider')) {
      const sliderNodes = document.querySelectorAll('.slider-container');
      let sliderInstances = [];

      sliderNodes.forEach(item => {
        const sliderNode = item.querySelector('.slider');

        if (sliderNode) {
          let gutter = 16;

          if (sliderNode.id === 'products-slider') {
            gutter = 32;
          }

          sliderInstances[sliderNode.id] = tns({
            container: sliderNode,
            loop: false,
            rewind: true,
            mouseDrag: false,
            preventScrollOnTouch: true,
            nav: true,
            navPosition: 'bottom',
            gutter: 16,
            items: 1,
            controls: false,
            controlsContainer: document.getElementById(sliderNode.id + '-controls'),
            responsive: {380: {items: 2}, 640: {items: 3}, 768: {controls: true, nav: false}, 992: {items: 4}, 1200: {gutter: gutter}}
          });
        }
      });
    }

    if (document.getElementById('promotions-slider')) {
      const promotionsSliderNode = document.getElementById('promotions-slider');
      let promotionsSliderInstance;

      promotionsSliderInstance = tns({
        container: promotionsSliderNode,
        loop: false,
        rewind: true,
        mouseDrag: false,
        autoplay: true,
        autoplayTimeout: 5000,
        preventScrollOnTouch: true,
        nav: false,
        navPosition: 'bottom',
        gutter: 0,
        items: 1,
        controls: true,
        controlsContainer: document.getElementById(promotionsSliderNode.id + '-controls'),
        responsive: {992: {nav: true}},
      });

      promotionsSliderInstance.events.on('indexChanged', () => {
        setTimeout(() => {
          promotionsSliderInstance.play();
        }, 1);
      });
    }
  }
});
