<section class="section section--dark section--bb section--mt">
  <div class="container">
    <div class="advantages">
      <div class="advantages__item">
        <div class="advantages__item-icon">
          <svg aria-label="Millennium in Twitter">
            <use xlink:href="#svg-icon-bonus"></use>
          </svg>
        </div>
        <div class="advantages__item-title">Bonus program</div>
        <div class="advantages__item-text">Sint occaecat cupidatat non</div>
      </div>
      <div class="advantages__item">
        <div class="advantages__item-icon">
          <svg aria-label="Millennium in Twitter">
            <use xlink:href="#svg-icon-support"></use>
          </svg>
        </div>
        <div class="advantages__item-title">24/7 support</div>
        <div class="advantages__item-text">Eiusmod tempor incididunt ut labore</div>
      </div>
      <div class="advantages__item">
        <div class="advantages__item-icon">
          <svg aria-label="Millennium in Twitter">
            <use xlink:href="#svg-icon-refund"></use>
          </svg>
        </div>
        <div class="advantages__item-title">Free 14-days refund</div>
        <div class="advantages__item-text">Nostrud exercitation ullamco laboris</div>
      </div>
      <div class="advantages__item">
        <div class="advantages__item-icon">
          <svg aria-label="Millennium in Twitter">
            <use xlink:href="#svg-icon-delivery"></use>
          </svg>
        </div>
        <div class="advantages__item-title">Worldwide shipping</div>
        <div class="advantages__item-text">Duis aute irure dolor in reprehenderit</div>
      </div>
    </div>
  </div>
</section>

<section class="section section--dark section--mb">
  <div class="container">
    <div class="subscription">
      <div class="subscription__text">Subscribe to our newsletter and be the first to receive the latest fashion news, promotions and more!</div>
      <form action="#" class="subscription__form">
        <input type="email" placeholder="Enter E-mail address...">
        <button type="submit">Subscribe</button>
      </form>
    </div>
  </div>
</section>
