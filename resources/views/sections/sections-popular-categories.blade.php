<section class="section--mt section--mb">
  <div class="container">

    <h1 class="section-title"><span>Popular Categories</span></h1>

    <div class="slider-container categories-slider">
      <div class="slider" id="categories-slider">
        <div>
          <a href="#"
             class="category-card">
            <div class="category-card__image">
              <img class="image-cover"
                   src="//lp2.hm.com/hmgoepprod?set=source[/d8/51/d851c87b14145a78f54f8188aea6231ed8bc83df.jpg],origin[dam],category[men_cardigansjumpers_jumpers],type[LOOKBOOK],res[y],hmver[1]&call=url[file:/product/main]"
                   alt="">
            </div>
            <div class="category-card__content">
              <div class="category-card__title">Sweaters</div>
              <div class="category-card__count">138 items</div>
            </div>
          </a>
        </div>
        <div>
          <a href="#"
             class="category-card">
            <div class="category-card__image">
              <img class="image-cover"
                   src="//lp2.hm.com/hmgoepprod?set=source[/e2/7a/e27a240133a9adc79527d92757ccb6896d3ec267.jpg],origin[dam],category[men_bags],type[LOOKBOOK],res[y],hmver[1]&call=url[file:/product/main]"
                   alt="">
            </div>
            <div class="category-card__content">
              <div class="category-card__title">Bags</div>
              <div class="category-card__count">168 items</div>
            </div>
          </a>
        </div>
        <div>
          <a href="#"
             class="category-card">
            <div class="category-card__image">
              <img class="image-cover"
                   src="https://lp2.hm.com/hmgoepprod?set=source[/b9/51/b951156cd41d9389b42e6dc426f54c8f1e3f5580.jpg],origin[dam],category[men_tshirtstanks_shortsleeve],type[LOOKBOOK],res[y],hmver[1]&call=url[file:/product/main]"
                   alt="">
            </div>
            <div class="category-card__content">
              <div class="category-card__title">T-shirts</div>
              <div class="category-card__count">258 items</div>
            </div>
          </a>
        </div>
        <div>
          <a href="#"
             class="category-card">
            <div class="category-card__image">
              <img class="image-cover"
                   src="https://lp2.hm.com/hmgoepprod?set=source[/4a/91/4a9120d37d9a4aa4e899cdefa68d3d00dc964a27.jpg],origin[dam],category[men_blazerssuits_blazers],type[LOOKBOOK],res[y],hmver[1]&call=url[file:/product/main]"
                   alt="">
            </div>
            <div class="category-card__content">
              <div class="category-card__title">Suits</div>
              <div class="category-card__count">65 items</div>
            </div>
          </a>
        </div>
        <div>
          <a href="#"
             class="category-card">
            <div class="category-card__image">
              <img class="image-cover"
                   src="//lp2.hm.com/hmgoepprod?set=source[/e5/60/e560388df08caf303fddf6861112bca921c3553a.jpg],origin[dam],category[],type[LOOKBOOK],res[y],hmver[1]&call=url[file:/product/main]"
                   alt="">
            </div>
            <div class="category-card__content">
              <div class="category-card__title">Jackets</div>
              <div class="category-card__count">92 items</div>
            </div>
          </a>
        </div>
      </div>
      <div class="slider__controls" id="categories-slider-controls">
        <button class="slider__arrow slider__arrow-prev"
                aria-label="{{ __('Перейти к предыдущему слайду') }}">
          <svg class="slider__arrow-svg">
            <use xlink:href="#svg-icon-arrow-left"></use>
          </svg>
        </button>
        <button class="slider__arrow slider__arrow-next"
                aria-label="{{ __('Перейти к следующему слайду') }}">
          <svg class="slider__arrow-svg">
            <use xlink:href="#svg-icon-arrow-left"></use>
          </svg>
        </button>
      </div>
    </div>
  </div>
</section>
