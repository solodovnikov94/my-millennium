<section class="section--mt section--mb">
  <div class="container">

    @isset($subtitle)
      <h3 class="section-subtitle">{{ $subtitle }}</h3>
    @endisset
    <h1 class="section-title"><span>{{ $title }}</span></h1>

    <div class="slider-container products-slider">
      <div class="slider" id="products-slider">
        <div>
          <product-card
            :secondary-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/91/da/91da676f779f12a170b5ce5a5e33c179ea129f51.jpg],origin[dam],category[],type[DESCRIPTIVESTILLLIFE],res[w],hmver[1]&call=url[file:/product/main]'"
            :main-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/17/d6/17d69fa9843d1a2aac86d06db6201b4eca5ca233.jpg],origin[dam],category[men_trousers_cargo],type[LOOKBOOK],res[w],hmver[1]&call=url[file:/product/main]'"
            :link="'#'"
            :label="'new'"
            :colors="['wheat', '#CF5151', 'midnightblue', '#1e1e1e', 'red', 'blue', 'green']"
            :title="'The North Face'"
            :subtitle="'Cashmere-blend Poncho'"
            :price="'69.99'"
          ></product-card>
        </div>
        <div>
          <product-card
            :secondary-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/4e/1f/4e1f281f04cc4722a2401fd75156993281b748fb.jpg],origin[dam],category[men_shorts_casual],type[DESCRIPTIVESTILLLIFE],res[w],hmver[1]&call=url[file:/product/main]'"
            :main-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/73/1b/731bb345b9aed5eca6ce2ae6d3c520b53d8d1999.jpg],origin[dam],category[men_shorts_casual],type[LOOKBOOK],res[m],hmver[1]&call=url[file:/product/main]'"
            :link="'#'"
            :colors="['#B1DACD', '#96ADD4']"
            :title="'Saint James'"
            :subtitle="'Fine-knit Sweater'"
            :price="'38.99'"
          ></product-card>
        </div>
        <div>
          <product-card
            :secondary-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/9e/fc/9efc3656eb630aca1e9ed58646138fed17be1f19.jpg],origin[dam],category[men_hoodiessweatshirts],type[DESCRIPTIVESTILLLIFE],res[w],hmver[1]&call=url[file:/product/main]'"
            :main-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/25/65/2565823ad966a821cb0e3ae085f8a99b66e8f35d.jpg],origin[dam],category[men_hoodiessweatshirts],type[LOOKBOOK],res[w],hmver[1]&call=url[file:/product/main]'"
            :link="'#'"
            :colors="['white', '#F4AB0E']"
            :title="'Vallier'"
            :subtitle="'Rib-knit Turtleneck Sweater'"
            :price="'9.99'"
          ></product-card>
        </div>
        <div>
          <product-card
            :secondary-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/24/38/24382bc384b162d8ff3e7f479e48eb144dee93bc.jpg],origin[dam],category[men_shirts_shortsleeved],type[DESCRIPTIVESTILLLIFE],res[w],hmver[1]&call=url[file:/product/main]'"
            :main-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/8f/d2/8fd20a43bd3a099b7b3ca4fa00635e10efec3ffb.jpg],origin[dam],category[men_shirts_shortsleeved],type[LOOKBOOK],res[m],hmver[1]&call=url[file:/product/main]'"
            :link="'#'"
            :colors="['#F0EEED', '#7C8C87', '#2B2C32', '#393A45', 'red']"
            :title="'Jaggad'"
            :subtitle="'Knit Sweater'"
            :price="'72.99'"
          ></product-card>
        </div>
        <div>
          <product-card
            :secondary-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/b8/a7/b8a7c235e14bffb45c11a638d5f2f9d2bae2c4a7.jpg],origin[dam],category[men_tshirtstanks_shortsleeve],type[DESCRIPTIVESTILLLIFE],res[w],hmver[1]&call=url[file:/product/main]'"
            :main-image="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/b2/35/b235b6a206fae1106c4f9f207b77cba593a80407.jpg],origin[dam],category[men_tshirtstanks_shortsleeve],type[LOOKBOOK],res[m],hmver[1]&call=url[file:/product/main]'"
            :link="'#'"
            :colors="['#1e1e1e', 'white']"
            :title="'Mountain Hardwear'"
            :subtitle="'Knit Mock-turtleneck Sweater'"
            :price="'17.49'"
          ></product-card>
        </div>
      </div>

      <div class="slider__controls" id="products-slider-controls">
        <button class="slider__arrow slider__arrow-prev"
                aria-label="{{ __('Перейти к предыдущему слайду') }}">
          <svg class="slider__arrow-svg">
            <use xlink:href="#svg-icon-arrow-left"></use>
          </svg>
        </button>
        <button class="slider__arrow slider__arrow-next"
                aria-label="{{ __('Перейти к следующему слайду') }}">
          <svg class="slider__arrow-svg">
            <use xlink:href="#svg-icon-arrow-left"></use>
          </svg>
        </button>
      </div>
    </div>
  </div>
</section>
