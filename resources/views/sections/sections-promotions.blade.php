<section class="section--mt section--mb">
  <div class="container">
    <div class="promotions">
      <div class="promotions__item">
        <a href="#"
           class="promotion">
          <div class="promotion__image">
            <img class="lazy image-cover"
                 v-lazy="'https://www2.hm.com/content/dam/campaign-men-s01/april-2020/3271e/3271E-2x3-mens-summer-polo-shirts.jpg'"
                 alt="">
          </div>
          <div class="promotion__content">
            <div class="promotion__content-top">
              <div class="promotion__description">Don't sweat it</div>
            </div>
            <div class="promotion__content-middle">
              <div class="promotion__title">Performance styles</div>
              <div class="promotion__subtitle">Duis aute irure dolor</div>
            </div>
            <div class="promotion__content-bottom">
              <span class="btn btn-light">Shop</span>
            </div>
          </div>
        </a>
      </div>
      <div class="promotions__item">
        <a href="#"
           class="promotion">
          <div class="promotion__image">
            <img class="lazy image-cover"
                 v-lazy="'https://www2.hm.com/content/dam/campaign-men-s01/april-2020/3271f/3271F-2x3mens-resort-shirts.jpg'"
                 alt="">
          </div>
          <div class="promotion__content">
            <div class="promotion__content-top">
              <div class="promotion__description">Keep it shorts</div>
            </div>
            <div class="promotion__content-middle">
              <div class="promotion__title">Performance styles</div>
              <div class="promotion__subtitle">Nostrud exercitation ullamco</div>
            </div>
            <div class="promotion__content-bottom">
              <span class="btn btn-light">Shop</span>
            </div>
          </div>
        </a>
      </div>
      <div class="promotions__item">
        <a href="#"
           class="promotion">
          <div class="promotion__image">
            <img class="lazy image-cover"
                 v-lazy="'https://www2.hm.com/content/dam/campaign-men-s01/may-2020/3271m/3271M-2x3-mens-swim-shorts.jpg'"
                 alt="">
          </div>
          <div class="promotion__content">
            <div class="promotion__content-top">
              <div class="promotion__description">Our new collection</div>
            </div>
            <div class="promotion__content-middle">
              <div class="promotion__title">Introducing boat shoes</div>
              <div class="promotion__subtitle">Ullamco laboris nisi ut aliquip</div>
            </div>
            <div class="promotion__content-bottom">
              <span class="btn btn-light">Shop</span>
            </div>
          </div>
        </a>
      </div>
      <div class="promotions__item">
        <a href="#"
           class="promotion">
          <div class="promotion__image">
            <img class="lazy image-cover"
                 v-lazy="'https://www2.hm.com/content/dam/TOOLBOX/PRE_SEASON/2020_s1/april/3201B-TCM1096-SGI-MEN-3x2.jpg'"
                 alt="">
          </div>
          <div class="promotion__content">
            <div class="promotion__content-top">
              <div class="promotion__description">The new</div>
            </div>
            <div class="promotion__content-middle">
              <div class="promotion__title">Any-season Essential</div>
              <div class="promotion__subtitle">Aliquip nostrud exercitation</div>
            </div>
            <div class="promotion__content-bottom">
              <span class="btn btn-light">Shop</span>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
</section>
