<section class="section--mt section--mb">
  <div class="container">
    <div class="home-builder">
      <div class="home-builder__item">
        <a href="#" class="home-builder__link">
          <img class="lazy image-cover"
               v-lazy="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/22/24/222479663304bcc4e15ebf74303e509fe0fa34d4.jpg],origin[dam],category[ladies_basics_dressesskirts],type[LOOKBOOK],res[m],hmver[1]&call=url[file:/product/main]'"
               alt="">
          <div class="home-builder__title">Shop women</div>
        </a>
      </div>
      <div class="home-builder__item">
        <a href="#" class="home-builder__link">
          <img class="lazy image-cover"
               v-lazy="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/57/bb/57bb05dea6cadd956e2a3259bf0730e20965c64f.jpg],origin[dam],category[men_shorts],type[LOOKBOOK],res[m],hmver[1]&call=url[file:/product/main]'"
               alt="">
          <div class="home-builder__title">Shop men</div>
        </a>
      </div>
      <div class="home-builder__item">
        <a href="#" class="home-builder__link">
          <img class="lazy image-cover"
               v-lazy="'https://lp2.hm.com/hmgoepprod?set=quality[79],source[/e2/7e/e27e1786d002e4d4403b9afddfe877d4b49d23b2.jpg],origin[dam],category[kids_girl14y_trousersleggings],type[LOOKBOOK],res[m],hmver[1]&call=url[file:/product/main]'"
               alt="">
          <div class="home-builder__title">Shop children</div>
        </a>
      </div>
    </div>
  </div>
</section>
