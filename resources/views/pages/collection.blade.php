@extends('layouts.layouts-main')

@section('content')
  <section class="section--mb">

  </section>

  <!--Advantages and subscription section-->
  @include('sections.sections-advantages-subscription')

  <!--Instagram section-->
  @include('sections.sections-instagram')
@stop
