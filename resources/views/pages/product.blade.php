@extends('layouts.layouts-main')

@section('content')
  <section class="section--mb">

  </section>

  <!--Other also bought-->
  @include('sections.sections-products-slider', ['title' => 'Other also bought', 'subtitle' => 'Shop now'])

  <!--Advantages and subscription section-->
  @include('sections.sections-advantages-subscription')

  <!--Instagram section-->
  @include('sections.sections-instagram')
@stop
