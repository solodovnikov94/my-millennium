@extends('layouts.layouts-main')

@section('content')
  <section class="section--mb">
      <div class="slider-container promotions-slider-container">
        <div class="promotions-slider" id="promotions-slider">
          <div>
            <div class="promotion-slide">
              <div class="promotions-slide__image">
                <img class="image-cover lazy"
                     v-lazy="'https://www2.hm.com/content/dam/campaign-home-s01/may-2020/7071h/7071h-patio-perfection-3x2.jpg'"
                     alt="">
              </div>
              <div class="promotion-slide__content-container">
                <div class="container">
                  <div class="promotion-slide__content">
                    <div class="promotion-slide__count">
                      <span class="promotion-slide__count-current">01</span><span class="promotion-slide__count-all">/07</span>
                    </div>
                    <div class="promotion-slide__subtitle">Magazine</div>
                    <div class="promotion-slide__title">How to reinvent<br>your summer staples</div>
                    <div class="promotion-slide__button">
                      <a href="#" class="btn btn-light">Shop</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="promotion-slide">
              <div class="promotions-slide__image">
                <img class="image-cover lazy"
                     v-lazy="'https://www2.hm.com/content/dam/campaign-home-s01/april-2020/7071d/7071d-inspired-by-nature-3x2-teaser-02.jpg'"
                     alt="">
              </div>
              <div class="promotion-slide__content-container">
                <div class="container">
                  <div class="promotion-slide__content">
                    <div class="promotion-slide__count">
                      <span class="promotion-slide__count-current">02</span><span class="promotion-slide__count-all">/07</span>
                    </div>
                    <div class="promotion-slide__subtitle">SS 2020 Collection</div>
                    <div class="promotion-slide__title">New Arrivals</div>
                    <div class="promotion-slide__button">
                      <a href="#" class="btn btn-light">Shop</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="promotion-slide">
              <div class="promotions-slide__image">
                <img class="image-cover lazy"
                     v-lazy="'https://www2.hm.com/content/dam/TOOLBOX/LOCAL%20ACTIVITIES/2020_s01/june/UKONLY-DS1004-HOME-3X2.jpg'"
                     alt="">
              </div>
              <div class="promotion-slide__content-container">
                <div class="container">
                  <div class="promotion-slide__content">
                    <div class="promotion-slide__count">
                      <span class="promotion-slide__count-current">03</span><span class="promotion-slide__count-all">/07</span>
                    </div>
                    <div class="promotion-slide__subtitle">Discover our newest arrivals of lamps</div>
                    <div class="promotion-slide__title">Love at first light</div>
                    <div class="promotion-slide__button">
                      <a href="#" class="btn btn-light">Shop</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="promotion-slide">
              <div class="promotions-slide__image">
                <img class="image-cover lazy"
                     v-lazy="'https://www2.hm.com/content/dam/hm-magazine-2020/the-tryout/20_24_T_tryout_sustainable_summer_hm.jpg'"
                     alt="">
              </div>
              <div class="promotion-slide__content-container">
                <div class="container">
                  <div class="promotion-slide__content">
                    <div class="promotion-slide__count">
                      <span class="promotion-slide__count-current">04</span><span class="promotion-slide__count-all">/07</span>
                    </div>
                    <div class="promotion-slide__subtitle">Magazine</div>
                    <div class="promotion-slide__title">How to reinvent your<br>summer staples</div>
                    <div class="promotion-slide__button">
                      <a href="#" class="btn btn-light">Shop</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="promotion-slide">
              <div class="promotions-slide__image">
                <img class="image-cover lazy"
                     v-lazy="'https://lp2.hm.com/hmgoepprod?source=url[https://www2.hm.com/content/dam/hm-magazine-2019/experts-corner/20_01_A_Beauty_Foundations_0.jpg]&sink=format[jpeg],quality[70]'"
                     alt="">
              </div>
              <div class="promotion-slide__content-container">
                <div class="container">
                  <div class="promotion-slide__content">
                    <div class="promotion-slide__count">
                      <span class="promotion-slide__count-current">05</span><span class="promotion-slide__count-all">/07</span>
                    </div>
                    <div class="promotion-slide__subtitle">EXPERT’S CORNER</div>
                    <div class="promotion-slide__title">A beauty colour<br>on the rise</div>
                    <div class="promotion-slide__button">
                      <a href="#" class="btn btn-light">Shop</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="promotions-slider-controls">
          <div class="container">
            <div class="slider__controls" id="promotions-slider-controls">
              <button class="slider__arrow slider__arrow-prev"
                      aria-label="{{ __('Перейти к предыдущему слайду') }}">
                <svg class="slider__arrow-svg">
                  <use xlink:href="#svg-icon-arrow-left"></use>
                </svg>
              </button>
              <button class="slider__arrow slider__arrow-next"
                      aria-label="{{ __('Перейти к следующему слайду') }}">
                <svg class="slider__arrow-svg">
                  <use xlink:href="#svg-icon-arrow-left"></use>
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
  </section>

  <!--Promotions-->
  @include('sections.sections-promotions')

  <!--Popular categories-->
  @include('sections.sections-popular-categories')

  <!--Our bestsellers-->
  @include('sections.sections-products-slider', ['title' => 'Our bestsellers', 'subtitle' => 'Shop now'])

  <!--Advantages and subscription section-->
  @include('sections.sections-advantages-subscription')

  <!--Instagram section-->
  @include('sections.sections-instagram')
@stop
