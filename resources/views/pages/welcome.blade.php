@extends('layouts.layouts-main')

@section('content')
  <!--Home builder section-->
  @include('sections.sections-home-builder')

  <!--Advantages and subscription section-->
  @include('sections.sections-advantages-subscription')

  <!--Instagram section-->
  @include('sections.sections-instagram')
@stop
