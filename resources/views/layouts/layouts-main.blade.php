<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Millenium</title>
  <style>
    .preloader{position:fixed;top:0;left:0;right:0;bottom:0;z-index:9999;background:#1e1e1e;display:flex;align-items:center;justify-content:center;opacity:1;visibility:visible;transition:opacity .3s ease-out,visibility 0s linear .3s}.preloader.active{opacity:0;visibility:hidden}.preloader:after{content:"";display:block;width:40px;height:40px;border:4px solid;border-radius:50%;border-color:#fff #fff transparent transparent;-webkit-animation:preloader .6s linear infinite;animation:preloader .6s linear infinite}@-webkit-keyframes preloader{from{transform:rotate(0)}to{transform:rotate(360deg)}}@keyframes preloader{from{transform:rotate(0)}to{transform:rotate(360deg)}}
  </style>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora|Rubik:400,500&display=swap&subset=cyrillic">
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
<div class="wrapper" id="app">
  <div class="preloader" id="preloader"></div>

  <div class="wrapper__content">
    <div class="wrapper__header">
      <header-component></header-component>
    </div>
    <main>
      @yield('content')
    </main>
  </div>
  <footer-component></footer-component>
</div>

@if(file_exists(public_path().'/images/sprite.svg')){!! file_get_contents(public_path().'/images/sprite.svg') !!}@endif

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
