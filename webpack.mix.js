let mix = require('laravel-mix');

mix
  .js('resources/js/app.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css')
  .setPublicPath('public/')
  .browserSync('millenium.loc')
  .disableNotifications()
  .options({processCssUrls: false});

if (mix.inProduction()) {
  const SvgStore = require('webpack-svgstore-plugin');

  mix.webpackConfig({
    plugins: [
      new SvgStore({
        svgoOptions: {
          plugins: [
            {removeTitle: true}
          ]
        },
        prefix: 'svg-icon-'
      })
    ]
  });

  mix
  // .version()
  // .sourceMaps()
    .options({
      autoprefixer: {
        options: {
          browsers: [
            'last 5 versions'
          ]
        }
      }
    });
}
