<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {return view('pages.welcome');});
Route::get('/category', function () {return view('pages.category');});
Route::get('/product', function () {return view('pages.product');});
Route::get('/collection', function () {return view('pages.collection');});
