(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/components/footer"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/FooterComponent.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/vue/components/FooterComponent.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "FooterComponent",
  data: function data() {
    return {
      photos: [],
      isLoaded: false,
      loading: false
    };
  },
  methods: {
    toggleMenu: function toggleMenu(event) {
      if (window.innerWidth < 768) {
        event.preventDefault();
        var menu = event.currentTarget.parentNode;
        var list = event.currentTarget.nextElementSibling;
        menu.classList.toggle('active');

        if (list.style.maxHeight) {
          list.style.maxHeight = null;
        } else {
          list.style.maxHeight = list.scrollHeight + "px";
        }
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/FooterComponent.vue?vue&type=template&id=7785b7b0&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/vue/components/FooterComponent.vue?vue&type=template&id=7785b7b0& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "wrapper__footer" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "footer__line footer__top" }, [
          _vm._m(0),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "footer__socials" }, [
            _c("a", { attrs: { href: "#" } }, [
              _c("svg", { attrs: { "aria-label": "Millennium in Facebook" } }, [
                _c("use", { attrs: { "xlink:href": "#svg-icon-facebook" } })
              ])
            ]),
            _vm._v(" "),
            _c("a", { attrs: { href: "#" } }, [
              _c("svg", { attrs: { "aria-label": "Millennium in Twitter" } }, [
                _c("use", { attrs: { "xlink:href": "#svg-icon-twitter" } })
              ])
            ]),
            _vm._v(" "),
            _c("a", { attrs: { href: "#" } }, [
              _c(
                "svg",
                { attrs: { "aria-label": "Millennium in Instagram" } },
                [_c("use", { attrs: { "xlink:href": "#svg-icon-instagram" } })]
              )
            ]),
            _vm._v(" "),
            _c("a", { attrs: { href: "#" } }, [
              _c("svg", { attrs: { "aria-label": "Millennium in Youtube" } }, [
                _c("use", { attrs: { "xlink:href": "#svg-icon-youtube" } })
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer__main" }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "footer__line footer__menu" }, [
            _c("div", { staticClass: "footer__menu-column" }, [
              _c("div", { staticClass: "footer-menu" }, [
                _c(
                  "div",
                  {
                    staticClass: "footer-menu__title",
                    on: {
                      click: function($event) {
                        return _vm.toggleMenu($event)
                      }
                    }
                  },
                  [_vm._v("Account")]
                ),
                _vm._v(" "),
                _vm._m(2)
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "footer__menu-column" }, [
              _c("div", { staticClass: "footer-menu" }, [
                _c(
                  "div",
                  {
                    staticClass: "footer-menu__title",
                    on: {
                      click: function($event) {
                        return _vm.toggleMenu($event)
                      }
                    }
                  },
                  [_vm._v("For customers")]
                ),
                _vm._v(" "),
                _vm._m(3)
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "footer__menu-column" }, [
              _c("div", { staticClass: "footer-menu" }, [
                _c(
                  "div",
                  {
                    staticClass: "footer-menu__title",
                    on: {
                      click: function($event) {
                        return _vm.toggleMenu($event)
                      }
                    }
                  },
                  [_vm._v("Millennium")]
                ),
                _vm._v(" "),
                _vm._m(4)
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "footer__menu-column" }, [
              _c("div", { staticClass: "footer-menu" }, [
                _c(
                  "div",
                  {
                    staticClass: "footer-menu__title",
                    on: {
                      click: function($event) {
                        return _vm.toggleMenu($event)
                      }
                    }
                  },
                  [_vm._v("Kyiv office")]
                ),
                _vm._v(" "),
                _vm._m(5)
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "footer-menu" }, [
                _c(
                  "div",
                  {
                    staticClass: "footer-menu__title",
                    on: {
                      click: function($event) {
                        return _vm.toggleMenu($event)
                      }
                    }
                  },
                  [_vm._v("Warsaw office")]
                ),
                _vm._v(" "),
                _vm._m(6)
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _vm._m(7)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "footer__phone" }, [
      _c("div", { staticClass: "footer__phone-number" }, [
        _c("a", { attrs: { href: "tel:+38 (050) 100 10 10" } }, [
          _vm._v("+38 (050) 100 10 10")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer__phone-text" }, [
        _vm._v("Monday to Friday, 11am - 5pm")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "footer__logo-container" }, [
      _c("a", { staticClass: "footer__logo", attrs: { href: "#" } }, [
        _c("span", { staticClass: "logo" }, [_vm._v("Millennium")]),
        _vm._v(" "),
        _c("span", { staticClass: "footer__logo-text" }, [
          _vm._v("eCommerce Template")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "footer-menu__items" }, [
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Orders")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Addresses")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Account details")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Wishlist")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "footer-menu__items" }, [
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Contacts us")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Payment & Delivery")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Returns & Refunds")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Terms & Conditions")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Help & FAQs")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "footer-menu__items" }, [
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("About us")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Stores")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Join the Team")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__item" }, [
        _c("a", { staticClass: "footer-menu__link", attrs: { href: "#" } }, [
          _vm._v("Privacy & Cookie policy")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "footer-menu__items" }, [
      _c("div", { staticClass: "footer-menu__text" }, [
        _vm._v("Khreshchatyk St., 29/1, Kiev 80382 Ukraine")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__text" }, [
        _vm._v("+38 (050) 110 10 10")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "footer-menu__items" }, [
      _c("div", { staticClass: "footer-menu__text" }, [
        _vm._v("Jana Pawla Avenue, 82, Warsaw 00-195, Poland")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "footer-menu__text" }, [
        _vm._v("+48 20 200 20 20")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "footer__copy" }, [
        _vm._v(
          "\n        2020, Millennium - Premium eCommerce Template\n      "
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/vue/components/FooterComponent.vue":
/*!*********************************************************!*\
  !*** ./resources/js/vue/components/FooterComponent.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FooterComponent_vue_vue_type_template_id_7785b7b0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FooterComponent.vue?vue&type=template&id=7785b7b0& */ "./resources/js/vue/components/FooterComponent.vue?vue&type=template&id=7785b7b0&");
/* harmony import */ var _FooterComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FooterComponent.vue?vue&type=script&lang=js& */ "./resources/js/vue/components/FooterComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _FooterComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _FooterComponent_vue_vue_type_template_id_7785b7b0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _FooterComponent_vue_vue_type_template_id_7785b7b0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/components/FooterComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/vue/components/FooterComponent.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/vue/components/FooterComponent.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FooterComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./FooterComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/FooterComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FooterComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/vue/components/FooterComponent.vue?vue&type=template&id=7785b7b0&":
/*!****************************************************************************************!*\
  !*** ./resources/js/vue/components/FooterComponent.vue?vue&type=template&id=7785b7b0& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FooterComponent_vue_vue_type_template_id_7785b7b0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./FooterComponent.vue?vue&type=template&id=7785b7b0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/FooterComponent.vue?vue&type=template&id=7785b7b0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FooterComponent_vue_vue_type_template_id_7785b7b0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FooterComponent_vue_vue_type_template_id_7785b7b0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);