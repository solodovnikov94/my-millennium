(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/components/instagram-list"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/InstagramList.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/vue/components/InstagramList.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "InstagramList",
  data: function data() {
    return {
      photos: [],
      isLoadingPhotos: false
    };
  },
  created: function created() {
    this.pushItems();
  },
  methods: {
    pushItems: function pushItems() {
      var count = this.photos.length;

      for (var i = count; i < count + 4; i++) {
        this.photos.push({
          url: 'https://picsum.photos/357/357?random=' + i,
          likes: '11k',
          title: 'Image caption'
        });
      }
    },
    loadMore: function loadMore() {
      var _this = this;

      var delay = Math.floor(Math.random() * 500);
      this.isLoadingPhotos = true;
      setTimeout(function () {
        _this.pushItems();

        _this.isLoadingPhotos = false;
      }, delay);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/InstagramList.vue?vue&type=template&id=471fb804&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/vue/components/InstagramList.vue?vue&type=template&id=471fb804& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "section-subtitle" }, [_vm._v("Instagram")]),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "ins" },
      _vm._l(_vm.photos, function(photo) {
        return _c("div", { staticClass: "ins__item" }, [
          _c(
            "a",
            { staticClass: "ins__item-link", attrs: { href: photo.url } },
            [
              _c("img", {
                directives: [
                  {
                    name: "lazy",
                    rawName: "v-lazy",
                    value: photo.url,
                    expression: "photo.url"
                  }
                ],
                staticClass: "ins__item-image image-cover lazy",
                attrs: { alt: photo.title }
              }),
              _vm._v(" "),
              _c("span", { staticClass: "ins__item-likes" }, [
                _vm._v(_vm._s(photo.likes) + " Likes")
              ])
            ]
          )
        ])
      }),
      0
    ),
    _vm._v(" "),
    _c("div", { staticClass: "m-t text-center" }, [
      _c(
        "div",
        {
          staticClass: "btn-container",
          class: { "loader loader-btn": _vm.isLoadingPhotos }
        },
        [
          _c("button", { staticClass: "btn", on: { click: _vm.loadMore } }, [
            _vm._v("More...")
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h1", { staticClass: "section-title section-title--instagram" }, [
      _c("span", [_vm._v("MillenniumShop")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/vue/components/InstagramList.vue":
/*!*******************************************************!*\
  !*** ./resources/js/vue/components/InstagramList.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _InstagramList_vue_vue_type_template_id_471fb804___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./InstagramList.vue?vue&type=template&id=471fb804& */ "./resources/js/vue/components/InstagramList.vue?vue&type=template&id=471fb804&");
/* harmony import */ var _InstagramList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./InstagramList.vue?vue&type=script&lang=js& */ "./resources/js/vue/components/InstagramList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _InstagramList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _InstagramList_vue_vue_type_template_id_471fb804___WEBPACK_IMPORTED_MODULE_0__["render"],
  _InstagramList_vue_vue_type_template_id_471fb804___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/components/InstagramList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/vue/components/InstagramList.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/vue/components/InstagramList.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_InstagramList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./InstagramList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/InstagramList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_InstagramList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/vue/components/InstagramList.vue?vue&type=template&id=471fb804&":
/*!**************************************************************************************!*\
  !*** ./resources/js/vue/components/InstagramList.vue?vue&type=template&id=471fb804& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_InstagramList_vue_vue_type_template_id_471fb804___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./InstagramList.vue?vue&type=template&id=471fb804& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/InstagramList.vue?vue&type=template&id=471fb804&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_InstagramList_vue_vue_type_template_id_471fb804___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_InstagramList_vue_vue_type_template_id_471fb804___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);