(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/components/header"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/HeaderComponent.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/vue/components/HeaderComponent.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "HeaderComponent",
  components: {
    MobileMenu: function MobileMenu() {
      return {
        component: __webpack_require__.e(/*! import() | js/components/mobile-menu */ "js/components/mobile-menu").then(__webpack_require__.bind(null, /*! ./MobileMenu */ "./resources/js/vue/components/MobileMenu.vue"))
      };
    }
  },
  data: function data() {
    return {
      wishList: 0,
      cart: 0,
      isOpenedMobileMenu: false,
      isOpenedMobileSearch: false
    };
  },
  mounted: function mounted() {
    this.initFloatedHeader();
    this.closeMobileMenu(); // fetch('../data/products.json')
    // 	.then(response => response.json())
    // 	.then(json => console.log(json))
  },
  methods: {
    initFloatedHeader: function initFloatedHeader() {
      var c = 0,
          currentScrollTop = 0;
      var header = document.getElementById('header');
      window.addEventListener('scroll', function () {
        var scrollFromTop = window.scrollY;
        var headerHeight = header.offsetHeight;
        currentScrollTop = scrollFromTop;

        if (window.innerWidth >= 992) {
          if (currentScrollTop > headerHeight - 5) {
            header.style.transform = "translateY(-" + (headerHeight - 5) + "px)";
            header.classList.add('without-transition');
            setTimeout(function () {
              header.classList.add("fixed");
              header.classList.remove("without-transition");
            }, 1);
          } else {
            header.classList.remove("fixed");
            header.classList.add("without-transition");
            header.style.transform = "translateY(0px)";
          }
        } else {
          if (currentScrollTop > headerHeight && c < currentScrollTop) {
            header.style.transform = "translateY(-" + headerHeight + "px)";
            setTimeout(function () {
              header.classList.add("fixed");
            }, 1);
          } else {
            header.style.transform = "translateY(0px)";
          }
        }

        c = currentScrollTop;
      });
      header.addEventListener('mouseover', function () {
        if (header.classList.contains('fixed')) {
          header.style.transform = "translateY(0px)";
          header.classList.add('active');
        }
      });
      document.addEventListener('mouseover', function (e) {
        if (window.innerWidth >= 992) {
          if (!header.contains(e.target) && header.classList.contains('fixed')) {
            header.style.transform = "translateY(-" + (header.offsetHeight - 5) + "px)";
            header.classList.remove('active');
          } else {
            header.style.transform = "translateY(0px)";
          }
        }
      });
    },
    toggleHeaderSearch: function toggleHeaderSearch(e) {
      e.preventDefault();
      this.isOpenedMobileSearch = !this.isOpenedMobileSearch;
      var form = document.getElementById('header-floated-search');
      form.classList.toggle('active');
      form.querySelector('input').focus();
    },
    changeSubmenu: function changeSubmenu(e, activeSubmenu) {
      e.preventDefault();
      var menus = document.querySelectorAll('.header-menu__item');
      var activeMenu = event.currentTarget;
      var submenus = document.querySelectorAll('.header-submenu');
      var desiredSubmenu = document.querySelector(".header-submenu[data-submenu=\"".concat(activeSubmenu, "\"]"));

      if (!activeMenu.classList.contains('active') && submenus) {
        menus.forEach(function (menu) {
          return menu.classList.remove('active');
        });
        submenus.forEach(function (item) {
          return item.classList.remove('show');
        });
        setTimeout(function () {
          submenus.forEach(function (item) {
            return item.classList.remove('active');
          });
          desiredSubmenu.classList.add('active');
          setTimeout(function () {
            return desiredSubmenu.classList.add('show');
          }, 75);
        }, 75);
        activeMenu.classList.add('active');
      }
    },
    openMobileMenu: function openMobileMenu() {
      this.isOpenedMobileMenu = !this.isOpenedMobileMenu;

      if (document.getElementById('mobile-menu') && window.innerWidth < 992) {
        document.body.classList.toggle('no-scroll');
        document.getElementById('mobile-menu').classList.toggle('active');
      }
    },
    closeMobileMenu: function closeMobileMenu() {
      window.addEventListener("orientationchange", function () {
        var mobileMenu = document.getElementById('mobile-menu');

        if (mobileMenu.classList.contains('active') && document.body.classList.contains('no-scroll')) {
          setTimeout(function () {
            if (window.innerWidth >= 992) {
              document.body.classList.remove('no-scroll');
              mobileMenu.classList.remove('active');
            }
          }, 1);
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/HeaderComponent.vue?vue&type=template&id=3a80b804&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/vue/components/HeaderComponent.vue?vue&type=template&id=3a80b804& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "header", attrs: { id: "header" } }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "header__center" }, [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "header__center-content" }, [
              _c(
                "div",
                { staticClass: "header-controls header-mobile-controls" },
                [
                  _c(
                    "a",
                    {
                      staticClass: "header-controls__item",
                      attrs: { id: "header-bars" },
                      on: { click: _vm.openMobileMenu }
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass: "header-controls__item-icon menu",
                          class: { active: _vm.isOpenedMobileMenu }
                        },
                        [
                          _c(
                            "svg",
                            {
                              staticClass: "bars",
                              attrs: { "aria-label": "Open mobile menu" }
                            },
                            [
                              _c("use", {
                                attrs: { "xlink:href": "#svg-icon-bars" }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "svg",
                            {
                              staticClass: "times",
                              attrs: { "aria-label": "Close mobile menu" }
                            },
                            [
                              _c("use", {
                                attrs: { "xlink:href": "#svg-icon-times" }
                              })
                            ]
                          )
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      staticClass: "header-controls__item",
                      on: { click: _vm.toggleHeaderSearch }
                    },
                    [
                      _c("div", { staticClass: "header-controls__item-icon" }, [
                        _c("svg", { attrs: { "aria-label": "Open search" } }, [
                          _c("use", {
                            attrs: { "xlink:href": "#svg-icon-search" }
                          })
                        ])
                      ])
                    ]
                  )
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "header-menu" }, [
                _c(
                  "a",
                  {
                    staticClass: "header-menu__item active",
                    on: {
                      click: function($event) {
                        return _vm.changeSubmenu($event, "women")
                      }
                    }
                  },
                  [_c("span", [_vm._v("Women")])]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "header-menu__item",
                    on: {
                      click: function($event) {
                        return _vm.changeSubmenu($event, "men")
                      }
                    }
                  },
                  [_c("span", [_vm._v("Men")])]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "header-menu__item",
                    on: {
                      click: function($event) {
                        return _vm.changeSubmenu($event, "kids")
                      }
                    }
                  },
                  [_c("span", [_vm._v("Kids")])]
                )
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("div", { staticClass: "header-controls" }, [
                _c(
                  "a",
                  {
                    staticClass: "header-controls__item",
                    attrs: { href: "#", id: "header-likes-counter" }
                  },
                  [
                    _c("div", { staticClass: "header-controls__item-icon" }, [
                      _c("svg", { attrs: { "aria-label": "My wish list" } }, [
                        _c("use", { attrs: { "xlink:href": "#svg-icon-like" } })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "header-controls__item-text" }, [
                      _vm._v(_vm._s(_vm.wishList))
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "header-controls__item",
                    attrs: { href: "#", id: "header-cart-counter" }
                  },
                  [
                    _c("div", { staticClass: "header-controls__item-icon" }, [
                      _c("svg", { attrs: { "aria-label": "My cart" } }, [
                        _c("use", { attrs: { "xlink:href": "#svg-icon-cart" } })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "header-controls__item-text" }, [
                      _vm._v(_vm._s(_vm.cart))
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "header-controls__item",
                    attrs: { href: "#", id: "header-account" }
                  },
                  [
                    _c("div", { staticClass: "header-controls__item-icon" }, [
                      _c("svg", { attrs: { "aria-label": "My account" } }, [
                        _c("use", {
                          attrs: { "xlink:href": "#svg-icon-account" }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "header-controls__item-text" }, [
                      _vm._v("My account")
                    ])
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c(
            "form",
            {
              staticClass: "header-floated-search",
              attrs: { id: "header-floated-search" }
            },
            [
              _c("div", { staticClass: "container" }, [
                _c("div", { staticClass: "header-floated-search__content" }, [
                  _c(
                    "button",
                    {
                      staticClass: "header-floated-search__icon",
                      attrs: { type: "submit" }
                    },
                    [
                      _c("svg", { attrs: { "aria-label": "Search" } }, [
                        _c("use", {
                          attrs: { "xlink:href": "#svg-icon-search" }
                        })
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("input", {
                    staticClass: "header-floated-search__field",
                    attrs: { type: "search", placeholder: "Search for..." }
                  }),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "header-floated-search__close",
                      on: { click: _vm.toggleHeaderSearch }
                    },
                    [
                      _c("svg", { attrs: { "aria-label": "Close search" } }, [
                        _c("use", {
                          attrs: { "xlink:href": "#svg-icon-close" }
                        })
                      ])
                    ]
                  )
                ])
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "header__bottom" }, [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "header__bottom-content" }, [
              _c("div", { staticClass: "header__bottom-submenus" }, [
                _c(
                  "ul",
                  {
                    staticClass: "header-submenu active show",
                    attrs: { "data-submenu": "women" }
                  },
                  [
                    _vm._m(2),
                    _vm._v(" "),
                    _c("li", { staticClass: "header-submenu__item" }, [
                      _vm._m(3),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "header-submenu__item-content" },
                        [
                          _c("div", { staticClass: "container" }, [
                            _c("div", { staticClass: "submenu" }, [
                              _vm._m(4),
                              _vm._v(" "),
                              _vm._m(5),
                              _vm._v(" "),
                              _c("div", { staticClass: "submenu__column" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "submenu-best",
                                    attrs: { href: "#" }
                                  },
                                  [
                                    _c("img", {
                                      directives: [
                                        {
                                          name: "lazy",
                                          rawName: "v-lazy",
                                          value:
                                            "../images/tmp/header-best-1.png",
                                          expression:
                                            "'../images/tmp/header-best-1.png'"
                                        }
                                      ],
                                      staticClass:
                                        "submenu-best__image image-cover",
                                      attrs: { alt: "Dresses" }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(6)
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "submenu__column" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "submenu-best",
                                    attrs: { href: "#" }
                                  },
                                  [
                                    _c("img", {
                                      directives: [
                                        {
                                          name: "lazy",
                                          rawName: "v-lazy",
                                          value:
                                            "../images/tmp/header-best-2.png",
                                          expression:
                                            "'../images/tmp/header-best-2.png'"
                                        }
                                      ],
                                      staticClass:
                                        "submenu-best__image image-cover",
                                      attrs: { alt: "Dresses" }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(7)
                                  ]
                                )
                              ])
                            ])
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _vm._m(8),
                    _vm._v(" "),
                    _vm._m(9),
                    _vm._v(" "),
                    _vm._m(10),
                    _vm._v(" "),
                    _c("li", { staticClass: "header-submenu__item" }, [
                      _vm._m(11),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "header-submenu__item-content" },
                        [
                          _c("div", { staticClass: "container" }, [
                            _c("div", { staticClass: "submenu" }, [
                              _vm._m(12),
                              _vm._v(" "),
                              _c("div", { staticClass: "submenu__column" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "submenu-best",
                                    attrs: { href: "#" }
                                  },
                                  [
                                    _c("img", {
                                      directives: [
                                        {
                                          name: "lazy",
                                          rawName: "v-lazy",
                                          value:
                                            "../images/tmp/header-best-3.png",
                                          expression:
                                            "'../images/tmp/header-best-3.png'"
                                        }
                                      ],
                                      staticClass:
                                        "submenu-best__image image-cover",
                                      attrs: { alt: "Dresses" }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(13)
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "submenu__column" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "submenu-best",
                                    attrs: { href: "#" }
                                  },
                                  [
                                    _c("img", {
                                      directives: [
                                        {
                                          name: "lazy",
                                          rawName: "v-lazy",
                                          value:
                                            "../images/tmp/header-best-4.png",
                                          expression:
                                            "'../images/tmp/header-best-4.png'"
                                        }
                                      ],
                                      staticClass:
                                        "submenu-best__image image-cover",
                                      attrs: { alt: "Dresses" }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(14)
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "submenu__column" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "submenu-best",
                                    attrs: { href: "#" }
                                  },
                                  [
                                    _c("img", {
                                      directives: [
                                        {
                                          name: "lazy",
                                          rawName: "v-lazy",
                                          value:
                                            "../images/tmp/header-best-5.png",
                                          expression:
                                            "'../images/tmp/header-best-5.png'"
                                        }
                                      ],
                                      staticClass:
                                        "submenu-best__image image-cover",
                                      attrs: { alt: "Dresses" }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(15)
                                  ]
                                )
                              ])
                            ])
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _vm._m(16),
                    _vm._v(" "),
                    _vm._m(17)
                  ]
                ),
                _vm._v(" "),
                _c(
                  "ul",
                  {
                    staticClass: "header-submenu",
                    attrs: { "data-submenu": "men" }
                  },
                  [
                    _vm._m(18),
                    _vm._v(" "),
                    _vm._m(19),
                    _vm._v(" "),
                    _vm._m(20),
                    _vm._v(" "),
                    _vm._m(21),
                    _vm._v(" "),
                    _vm._m(22),
                    _vm._v(" "),
                    _vm._m(23),
                    _vm._v(" "),
                    _c("li", { staticClass: "header-submenu__item" }, [
                      _vm._m(24),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "header-submenu__item-content" },
                        [
                          _c("div", { staticClass: "container" }, [
                            _c("div", { staticClass: "submenu" }, [
                              _vm._m(25),
                              _vm._v(" "),
                              _vm._m(26),
                              _vm._v(" "),
                              _c("div", { staticClass: "submenu__column" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "submenu-best",
                                    attrs: { href: "#" }
                                  },
                                  [
                                    _c("img", {
                                      directives: [
                                        {
                                          name: "lazy",
                                          rawName: "v-lazy",
                                          value:
                                            "../images/tmp/header-best-1.png",
                                          expression:
                                            "'../images/tmp/header-best-1.png'"
                                        }
                                      ],
                                      staticClass:
                                        "submenu-best__image image-cover",
                                      attrs: { alt: "Dresses" }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(27)
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "submenu__column" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "submenu-best",
                                    attrs: { href: "#" }
                                  },
                                  [
                                    _c("img", {
                                      directives: [
                                        {
                                          name: "lazy",
                                          rawName: "v-lazy",
                                          value:
                                            "../images/tmp/header-best-2.png",
                                          expression:
                                            "'../images/tmp/header-best-2.png'"
                                        }
                                      ],
                                      staticClass:
                                        "submenu-best__image image-cover",
                                      attrs: { alt: "Dresses" }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(28)
                                  ]
                                )
                              ])
                            ])
                          ])
                        ]
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _vm._m(29)
              ]),
              _vm._v(" "),
              _c("form", { staticClass: "header-search" }, [
                _c(
                  "button",
                  {
                    staticClass: "header-search__icon",
                    attrs: { type: "submit" }
                  },
                  [
                    _c("svg", { attrs: { "aria-label": "Search" } }, [
                      _c("use", { attrs: { "xlink:href": "#svg-icon-search" } })
                    ])
                  ]
                ),
                _vm._v(" "),
                _c("input", {
                  staticClass: "header-search__field",
                  attrs: { type: "search", placeholder: "Search for..." }
                })
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("mobile-menu")
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "header__top" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "header__top-content" }, [
          _c("div", { staticClass: "header-phone" }, [
            _c("a", { attrs: { href: "tel:+38 (050) 100 10 10" } }, [
              _vm._v("+38 (050) 100 10 10")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "header-news" }, [
            _c(
              "a",
              { staticClass: "header-news__link", attrs: { href: "#" } },
              [
                _c(
                  "time",
                  {
                    staticClass: "header-news__date",
                    attrs: { datetime: "2020-02-05T11:00:00+03:00" }
                  },
                  [_vm._v("February, 05")]
                ),
                _vm._v(" "),
                _c("span", { staticClass: "header-news__title" }, [
                  _vm._v("Free shipping over $150 & free 14-days return")
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "header-languages" }, [
            _c("a", { staticClass: "location", attrs: { href: "#" } }, [
              _c("span", { staticClass: "location__region" }, [
                _c("span", { staticClass: "location__region-flag" }, [
                  _c("img", {
                    attrs: { src: "images/flags-gb.png", alt: "United Kingdom" }
                  })
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "location__region-title" }, [
                  _vm._v("United Kingdom")
                ])
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "location__language" }, [
                _vm._v("English")
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "location__currency" }, [_vm._v("USD")])
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "header-logo" }, [
      _c("a", { staticClass: "header-logo__link", attrs: { href: "/" } }, [
        _c("span", { staticClass: "logo" }, [_vm._v("Millennium")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        {
          staticClass: "header-submenu__item-title special",
          attrs: { href: "#" }
        },
        [_c("span", [_vm._v("Sale")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
      [_c("span", [_vm._v("New in")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu__column" }, [
      _c("div", { staticClass: "submenu-list" }, [
        _c("h6", { staticClass: "submenu-list__title" }, [
          _vm._v("Shop by category")
        ]),
        _vm._v(" "),
        _c("ul", { staticClass: "submenu-list__items" }, [
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Dresses")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Tops")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Jackets & Coats")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Jumpers & Cardigans")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Jeans")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Swimwear")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Loungewear")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Essentials")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("At Home")]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu__column" }, [
      _c("div", { staticClass: "submenu-list" }, [
        _c("h6", { staticClass: "submenu-list__title" }, [
          _vm._v("Shop by trend")
        ]),
        _vm._v(" "),
        _c("ul", { staticClass: "submenu-list__items" }, [
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Mood-Boosting Hues")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Neutrals")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("The New Green")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Broderie")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Animal")]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu-best__content" }, [
      _c("div", { staticClass: "submenu-best__title" }, [_vm._v("Dresses")]),
      _vm._v(" "),
      _c("div", { staticClass: "submenu-best__button" }, [
        _c("span", { staticClass: "btn" }, [_vm._v("Shop")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu-best__content" }, [
      _c("div", { staticClass: "submenu-best__title" }, [_vm._v("New in")]),
      _vm._v(" "),
      _c("div", { staticClass: "submenu-best__button" }, [
        _c("span", { staticClass: "btn" }, [_vm._v("Shop")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("Clothing")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("Shoes")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("Bags")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
      [_c("span", [_vm._v("Accessories")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu__column" }, [
      _c("div", { staticClass: "submenu-list" }, [
        _c("h6", { staticClass: "submenu-list__title" }, [
          _vm._v("Shop by category")
        ]),
        _vm._v(" "),
        _c("ul", { staticClass: "submenu-list__items" }, [
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("See all")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Backpacks")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Wallets")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Tights & socks")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Belts")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Jewellery")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Caps & hats")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Scarves & foulards")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Other")]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu-best__content" }, [
      _c("div", { staticClass: "submenu-best__title" }, [_vm._v("Belts")]),
      _vm._v(" "),
      _c("div", { staticClass: "submenu-best__button" }, [
        _c("span", { staticClass: "btn" }, [_vm._v("Shop")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu-best__content" }, [
      _c("div", { staticClass: "submenu-best__title" }, [_vm._v("Jewellery")]),
      _vm._v(" "),
      _c("div", { staticClass: "submenu-best__button" }, [
        _c("span", { staticClass: "btn" }, [_vm._v("Shop")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu-best__content" }, [
      _c("div", { staticClass: "submenu-best__title" }, [_vm._v("Glasses")]),
      _vm._v(" "),
      _c("div", { staticClass: "submenu-best__button" }, [
        _c("span", { staticClass: "btn" }, [_vm._v("Shop")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("Jewelry")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("Home&Gifts")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        {
          staticClass: "header-submenu__item-title special",
          attrs: { href: "#" }
        },
        [_c("span", [_vm._v("Sale")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("Home&Gifts")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("New in")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("Clothing")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("Bags")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "header-submenu__item" }, [
      _c(
        "a",
        { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
        [_c("span", [_vm._v("Jewelry")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
      [_c("span", [_vm._v("Shoes")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu__column" }, [
      _c("div", { staticClass: "submenu-list" }, [
        _c("h6", { staticClass: "submenu-list__title" }, [
          _vm._v("Shop by category")
        ]),
        _vm._v(" "),
        _c("ul", { staticClass: "submenu-list__items" }, [
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Dresses")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Tops")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Jackets & Coats")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Jumpers & Cardigans")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Jeans")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Swimwear")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Loungewear")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Essentials")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("At Home")]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu__column" }, [
      _c("div", { staticClass: "submenu-list" }, [
        _c("h6", { staticClass: "submenu-list__title" }, [
          _vm._v("Shop by trend")
        ]),
        _vm._v(" "),
        _c("ul", { staticClass: "submenu-list__items" }, [
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Mood-Boosting Hues")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Neutrals")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("The New Green")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Broderie")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "submenu-list__item" }, [
            _c(
              "a",
              { staticClass: "submenu-list__item-link", attrs: { href: "#" } },
              [_vm._v("Animal")]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu-best__content" }, [
      _c("div", { staticClass: "submenu-best__title" }, [_vm._v("Dresses")]),
      _vm._v(" "),
      _c("div", { staticClass: "submenu-best__button" }, [
        _c("span", { staticClass: "btn" }, [_vm._v("Shop")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submenu-best__content" }, [
      _c("div", { staticClass: "submenu-best__title" }, [_vm._v("New in")]),
      _vm._v(" "),
      _c("div", { staticClass: "submenu-best__button" }, [
        _c("span", { staticClass: "btn" }, [_vm._v("Shop")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "ul",
      { staticClass: "header-submenu", attrs: { "data-submenu": "kids" } },
      [
        _c("li", { staticClass: "header-submenu__item" }, [
          _c(
            "a",
            {
              staticClass: "header-submenu__item-title special",
              attrs: { href: "#" }
            },
            [_c("span", [_vm._v("Sale")])]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "header-submenu__item" }, [
          _c(
            "a",
            { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
            [_c("span", [_vm._v("New in")])]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "header-submenu__item" }, [
          _c(
            "a",
            { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
            [_c("span", [_vm._v("Clothing")])]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "header-submenu__item" }, [
          _c(
            "a",
            { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
            [_c("span", [_vm._v("Shoes")])]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "header-submenu__item" }, [
          _c(
            "a",
            { staticClass: "header-submenu__item-title", attrs: { href: "#" } },
            [_c("span", [_vm._v("Home&Gifts")])]
          )
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/vue/components/HeaderComponent.vue":
/*!*********************************************************!*\
  !*** ./resources/js/vue/components/HeaderComponent.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HeaderComponent_vue_vue_type_template_id_3a80b804___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HeaderComponent.vue?vue&type=template&id=3a80b804& */ "./resources/js/vue/components/HeaderComponent.vue?vue&type=template&id=3a80b804&");
/* harmony import */ var _HeaderComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HeaderComponent.vue?vue&type=script&lang=js& */ "./resources/js/vue/components/HeaderComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HeaderComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HeaderComponent_vue_vue_type_template_id_3a80b804___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HeaderComponent_vue_vue_type_template_id_3a80b804___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/vue/components/HeaderComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/vue/components/HeaderComponent.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/vue/components/HeaderComponent.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HeaderComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./HeaderComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/HeaderComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HeaderComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/vue/components/HeaderComponent.vue?vue&type=template&id=3a80b804&":
/*!****************************************************************************************!*\
  !*** ./resources/js/vue/components/HeaderComponent.vue?vue&type=template&id=3a80b804& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HeaderComponent_vue_vue_type_template_id_3a80b804___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./HeaderComponent.vue?vue&type=template&id=3a80b804& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/vue/components/HeaderComponent.vue?vue&type=template&id=3a80b804&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HeaderComponent_vue_vue_type_template_id_3a80b804___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HeaderComponent_vue_vue_type_template_id_3a80b804___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);